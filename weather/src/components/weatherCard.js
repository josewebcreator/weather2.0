import React from "react";

function WeatherCard(prompt){
    return(
        <div className="WeatherCard container">
            
            <div className="row">
                <div className="img-weather col-sm-4">
                    <img
                    src={`https://openweathermap.org/img/wn/10d@4x.png`}></img>
                </div>
                <div className="data-weather col-sm-8">
                    <h2>Zone: {prompt.name}</h2>
                    <h3>Weather Condition: {prompt.condition}</h3>
                    <h4>temp: {prompt.temp}</h4>
                </div>
            </div>  
            
        </div>
    );

}
export default WeatherCard;