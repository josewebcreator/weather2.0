import React, { useState } from 'react';
import './App.css';
import WeatherCard from './components/weatherCard';

function App() {

  const [weatherData, setWeatherData] = useState(null);

  const handlerForm = e=>{
    e.preventDefault();
      let lat = e.target.latitud.value;
      let lon = e.target.longitud.value
      //console.log(latitud+' '+longitud);
      fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=b273036a56e411eb648659ca43f71f54`)
      .then((response)=>{
        if(!response.ok){
          console.log(response.statusText);
        }
        return response.json();
      })
        .then(response=>{
          setWeatherData(response);
          console.log(weatherData)
        })
  }

  return (
    <div className="App">
      <div className="header-app container">
            
            <div className='row'>
                <div className='tittle col-lg-4 col-md-4 col-sm-12'>
                    Weather App
                </div>
                <div className='div-form col-lg-7 col-md-7  col-sm-12'>
                    <form className='form-form' onSubmit={handlerForm}>
                        <div className='input-group'>
                            <input type='number' className='form-control coordenada' name="latitud" placeholder='latitud' ></input>
                            <input type='number' className='form-control coordenada' name="longitud" placeholder='longitud'></input>
                            <input type='submit' className='btn btn-primary'></input>
                        </div>
                        
                    </form>
                </div>    
            </div>
      </div>

      {weatherData&&(
        <WeatherCard
          name={weatherData.name}
          icon = {weatherData.weather[0].icon}
          condition = {weatherData.weather[0].description}
          temp = {weatherData.main.temp}

        />
      )
      }
    </div>
  );
}

export default App;
